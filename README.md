# pt_iptv

## How to use

This is a repository to aggregate all Portuguese IPTV streams (Expansion to other countries can be discussed in the future).

ALL the streams are LEGAL AND FREE, obtained from sources obtainable from google.

## How to use

To use this playlist you need to use the following links in your favorite PVR client:

Playlist link: https://gitlab.com/tiagogaspar8/pt_iptv/~raw/main/playlist.m3u8


## Contributing

Create a merge request to this repo.

1. Always preform your changes aginst the most recent commit (If needed you can rebase your changes)
2. Test your changes before marking them as ready for merge
3. I do not accept contributions to add Ilegal streams to this repo, so please don't even try

## Support
If you need help, some stream stopped working, you found a better stream to replace an existing one (to improve it), etc. you can create an issue.
I WILL NOT BE PROVIDING TECH SUPPORT. If you need to learn how to use yout PVR/IPTV client either go to their wiki or google it.

## Project status
This is a project done on free time, it might take time for issues to be replied to or even solved, the same applies for the merge requests, please be patient.
